#FROM nginx:1.9.5
FROM debian:jessie
MAINTAINER Scandio GmbH

# Install wget and install/updates certificates
RUN apt-get update \
 && apt-get install -y -q --no-install-recommends \
    ca-certificates \
    wget \
    libpcre3-dev \
    build-essential \
    apache2-dev \
    libxml2-dev \
    clamav \
    clamav-daemon \
    libssl-dev \
    vim \
# && apt-get purge -y nginx \
# && apt-get -y autoremove --purge \
 && apt-get clean \
 && rm -r /var/lib/apt/lists/*

# Install Forego
RUN wget -q -P /usr/local/bin https://godist.herokuapp.com/projects/ddollar/forego/releases/current/linux-amd64/forego \
 && chmod u+x /usr/local/bin/forego

ENV DOCKER_GEN_VERSION 0.4.2

RUN wget -q https://github.com/jwilder/docker-gen/releases/download/$DOCKER_GEN_VERSION/docker-gen-linux-amd64-$DOCKER_GEN_VERSION.tar.gz \
 && tar -C /usr/local/bin -xvzf docker-gen-linux-amd64-$DOCKER_GEN_VERSION.tar.gz \
 && rm /docker-gen-linux-amd64-$DOCKER_GEN_VERSION.tar.gz

# Install nginx and modsecurity
COPY bootstrap.sh /sbin/
RUN mkdir -p /root/temp \
    && cd /root/temp \
    && wget -q http://nginx.org/download/nginx-1.9.5.tar.gz \
    && tar -zxf nginx-1.9.5.tar.gz \
    && wget -q https://www.modsecurity.org/tarball/2.9.0/modsecurity-2.9.0.tar.gz \
    && tar -zxf modsecurity-2.9.0.tar.gz \
    && cd /root/temp/modsecurity-2.9.0 \
    && ./configure --enable-standalone-module --disable-mlogc --with-debug \
    && make \
    && cd /root/temp/nginx-1.9.5 \
    && ./configure --sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/nginx.conf --prefix=/etc/nginx --pid-path=/var/run/nginx.pid --error-log-path=/var/log/nginx/error.log --with-debug --http-log-path=/var/log/nginx/access.log --lock-path=/var/run/nginx.lock --add-module=/root/temp/modsecurity-2.9.0/nginx/modsecurity --with-http_ssl_module --without-http_access_module --without-http_auth_basic_module --without-http_autoindex_module --without-http_empty_gif_module --without-http_referer_module --without-http_memcached_module --without-http_scgi_module --without-http_split_clients_module --without-http_ssi_module --without-http_uwsgi_module \
    && make \
    && make install \
    && apt-get purge -y build-essential \
    && apt-get -y autoremove \
    && cd /root/temp \
    && wget -q --content-disposition https://github.com/SpiderLabs/owasp-modsecurity-crs/tarball/master \
    && find ./ -maxdepth 1 -type f -name "*owasp-modsecurity*.tar.gz" -exec tar -zxf {} \; \
    && find ./ -maxdepth 1 -type d -name "*owasp-modsecurity*" -exec mv {} owasp-modsecurity-crs \; \
    && chmod +x /sbin/bootstrap.sh

# Copy nginx init script
COPY nginx.init /etc/init.d/
RUN mv /etc/init.d/nginx.init /etc/init.d/nginx \
    && chmod +x /etc/init.d/nginx

# Configure Nginx and apply fix for very long server names
RUN echo "daemon off;" >> /etc/nginx/nginx.conf \
 && sed -i 's/^http {/&\n\tserver_names_hash_bucket_size 128;/g' /etc/nginx/nginx.conf

 # copy initial versions of clamav virus DB
 COPY main.cvd /var/lib/clamav/
 COPY daily.cvd /var/lib/clamav/
 COPY bytecode.cvd /var/lib/clamav/

COPY . /app/
WORKDIR /app/

ENV DOCKER_HOST unix:///tmp/docker.sock

VOLUME ["/etc/nginx/certs"]

ENTRYPOINT ["/sbin/bootstrap.sh"]
#ENTRYPOINT ["/app/docker-entrypoint.sh"]
#CMD ["forego", "start", "-r"]
