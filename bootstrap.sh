#!/bin/bash

touchPath=/root/no-bootstrap
nginxCfgDir=/etc/nginx
modsecSrcDir=/root/temp/modsecurity-2.9.0
owaspDir=/root/temp/owasp-modsecurity-crs

if [ ! -f ${touchPath} ]; then
	touch ${touchPath}

	if [ ! -d ${modsecSrcDir} ]; then
		echo "modsecurity source directory ${modsecSrcDir} not found"
		exit 1
	fi

	if [ ! -d ${nginxSrcDir} ]; then
		echo "nginx source directory ${nginxSrcDir} not found"
		exit 1
	fi

	if [ ! -d ${owaspDir} ]; then
		echo "modsecurity rules directory ${owaspDir} not found"
		exit 1
	fi

	# copy unicode mapping file
	if [ ! -f ${modsecSrcDir}/unicode.mapping ]; then
		echo "unicode mapping file not found"
		exit 1
	fi

	if [ ! -f ${modsecSrcDir}/modsecurity.conf-recommended ]; then
		echo "modsecurity recommended setup config file not found"
		exit 1
	fi

	if [ ! -f /etc/init.d/nginx ]; then
		echo "nginx init script file not found"
		exit 1
	fi

	if [ ! -d ${nginxCfgDir}/conf.d ]; then
		mkdir -p ${nginxCfgDir}/conf.d
		chown www-data: ${nginxCfgDir}/conf.d
	fi

	if [ ! -d ${nginxCfgDir}/modsecurity.d ]; then
		mkdir -p ${nginxCfgDir}/modsecurity.d
		chown www-data: ${nginxCfgDir}/modsecurity.d
	fi

	echo "installing scan script"
	cp modsec-clamscan.pl /usr/sbin/
	chmod +x /usr/sbin/modsec-clamscan.pl

	echo "replacing user in nginx config file"
	sed -ri 's/#?user\s+nobody;/user www-data;/i' /etc/nginx/nginx.conf

	echo "replacing user and group in clamav config file"
	sed -ri 's/User\s+clamav/User www-data/g' /etc/clamav/clamd.conf
	sed -ri 's/LocalSocketGroup\s+clamav/LocalSocketGroup www-data/g' /etc/clamav/clamd.conf

	echo "copying modsecurity files from directory ${modsecSrcDir}"
	cp ${modsecSrcDir}/unicode.mapping ${nginxCfgDir}/modsecurity.d/
	cp ${modsecSrcDir}/modsecurity.conf-recommended ${nginxCfgDir}/modsecurity.d/modsecurity.conf

	echo "processing files from directory ${owaspDir}"
	cd ${owaspDir}
	for i in base_rules/*.data
	do
		cp ${i} ${nginxCfgDir}/modsecurity.d/
	done

	# put base rules from files in folder base_rules into single file modsecurity.conf
	#for i in base_rules/*.conf
	#do
		#echo "" >> ${nginxCfgDir}/modsecurity.d/modsecurity.conf
		#cat ${i} >> ${nginxCfgDir}/modsecurity.d/modsecurity.conf
	#done

	# add rule for Upload
	perl -pi -e 's~SecRuleEngine\s+DetectionOnly~SecRuleEngine On\nSecRule FILES_TMPNAMES "\@inspectFile /usr/sbin/modsec-clamscan.pl" "id:123456789,phase:2,status:403,log,deny,t:none"\n~i' ${nginxCfgDir}/modsecurity.d/modsecurity.conf
	perl -pi -e 's~#\s*SecUploadKeepFiles\s+RelevantOnly~SecUploadKeepFiles RelevantOnly~i' ${nginxCfgDir}/modsecurity.d/modsecurity.conf
	perl -pi -e 's~#SecUploadDir.+?\n~SecUploadDir /tmp~i' ${nginxCfgDir}/modsecurity.d/modsecurity.conf
	perl -pi -e 's~SecRequestBodyLimit\s+\d+~SecRequestBodyLimit 33554432~i' ${nginxCfgDir}/modsecurity.d/modsecurity.conf
	perl -pi -e 's~#SecDebugLog /.+~SecDebugLog /var/log/nginx/modsec_debug.log~i' ${nginxCfgDir}/modsecurity.d/modsecurity.conf
	perl -pi -e 's~#SecDebugLogLevel.+~SecDebugLogLevel 0~i' ${nginxCfgDir}/modsecurity.d/modsecurity.conf
	perl -pi -e 's~SecAuditLog /var/log/modsec_audit.log~SecAuditLog /var/log/nginx/modsec_audit.log~' ${nginxCfgDir}/modsecurity.d/modsecurity.conf

	# activate aggressive mode of modsecurity
	echo "activating aggressive mode of modsecurity"
	perl -pi -e 's~SecRuleEngine DetectionOnly~SecRuleEngine On~i' ${nginxCfgDir}/modsecurity.d/modsecurity.conf

	echo "adding modsecurity module config load to nginx.conf and load of conf.d files"
	perl -pi -e 's~^([^#]*[ \t]*)location\s+/\s+\{\s*\n~${1}location / \{\n\t\tModSecurityEnabled on;\n\t\tModSecurityConfig /etc/nginx/modsecurity.d/modsecurity.conf;\n~gsi' /etc/nginx/nginx.conf
	perl -pi -001 -e 's~\n([ \t]*)http\s+\{\s*\n~\n${1}http \{\n${1}\tinclude /etc/nginx/conf.d/*.conf;\n~gsi' /etc/nginx/nginx.conf

#	echo "copying nginx.conf file from temp directory ${tempDir} to nginx config directory ${nginxCfgDir}"
#	cp ${tempDir}/nginx.conf ${nginxCfgDir}/

	if [ ! -d /var/log/nginx ]; then
		mkdir -p /var/log/nginx
	fi
	chown -R www-data: /var/log/nginx

	touch /var/log/nginx/modsec_audit.log
	touch /var/log/nginx/modsec_debug.log
	chown www-data: /var/log/nginx/modsec_*.log

	chown -R www-data: /var/log/clamav
	# clamav database
	#freshclam
	#wget -q -O /var/lib/clamav/main.cvd http://database.clamav.net/main.cvd
	#wget -q -O /var/lib/clamav/daily.cvd http://database.clamav.net/daily.cvd
	#wget -q -O /var/lib/clamav/bytecode.cvd http://database.clamav.net/bytecode.cvd
	chown -R www-data: /var/lib/clamav
	service clamav-daemon restart

#	/etc/init.d/nginx start

#	tail -F /var/log/nginx/error.log
fi

# remove temp folder
if [ -d /root/temp ]; then
	rm -rf /root/temp
fi

#exit 0
#forego start -r
/app/docker-entrypoint.sh forego start -f /app/Procfile -r
